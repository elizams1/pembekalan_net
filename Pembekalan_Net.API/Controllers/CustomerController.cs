﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pembekalan_Net.API.Services;
using Pembekalan_Net.DataModel;
using Pembekalan_Net.ViewModel;

namespace Pembekalan_Net.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        
        private VMResponse response =  new VMResponse();
        public CustomerServices customerServices;
        private int IdUser = 1;

        public CustomerController(CustomerServices _customerServices)
        {
            customerServices = _customerServices;
        }

        [HttpGet("[action]")]
        public List<VMTblCustomer> GetAllData()
        {
            List<VMTblCustomer> data= customerServices.GetAllData();
            return data;
        }

        [HttpGet("[action]/{email?}/{id?}")]
        public bool CheckEmailIsExist(string email, int id)
        {
            bool data = customerServices.CheckEmailIsExist(email, id);
            return data;
        }

        [HttpGet("[action]/{id?}")]
        public VMTblCustomer? GetDataById(int id)
        {
            VMTblCustomer? data = customerServices.GetDataById(id);
            return data;
        }

        [HttpPost("[action]")]
        public VMResponse SaveData(TblCustomer data)
        {
            VMResponse response = customerServices.SaveData(data);
            
            return response;
        }

        [HttpPut("[action]")]
        public VMResponse EditData(TblCustomer data)
        {
            VMResponse response = customerServices.EditData(data);

            return response;
        }

        [HttpDelete("[action]/{id?}")]
        public VMResponse DeleteData(int id)
        {
            VMResponse response = customerServices.DeleteData(id);

            return response;
        }

        [HttpPost("[action]")]
        public VMResponse MultipleDeleteData(List<int> listId)
        {
            VMResponse response = customerServices.MultipleDeleteData(listId);
            return response;
        }
    }
}
