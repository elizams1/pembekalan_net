﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pembekalan_Net.API.Services;
using Pembekalan_Net.DataModel;
using Pembekalan_Net.ViewModel;

namespace Pembekalan_Net.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        //private readonly NewNetContext db;
        private VMResponse response =  new VMResponse();
        private readonly RoleServices roleServices;
       
        private int IdUser = 1;

        public RoleController( RoleServices _roleServices)
        {
            roleServices = _roleServices;
            
        }

        [HttpGet("[action]")]
        public List<TblRole> GetAllData()
        {
            List<TblRole> data= roleServices.GetAllData();
            return data;
        }

        [HttpGet("[action]/{name?}/{id?}")]
        public bool CheckNameIsExist(string name, int id)
        {
            bool data = roleServices.CheckNameIsExist(name, id);
            return data;
        }

        [HttpGet("[action]/{id?}")]
        public TblRole GetDataById(int id)
        {
            TblRole data = roleServices.GetDataById(id);
            return data;
        }

        [HttpPost("[action]")]
        public VMResponse SaveData(TblRole data)
        {
            VMResponse response = roleServices.SaveData(data);
            
            return response;
        }

        [HttpPut("[action]")]
        public VMResponse EditData(TblRole data)
        {
            VMResponse response = roleServices.EditData(data);

            return response;
        }

        [HttpDelete("[action]/{id?}")]
        public VMResponse DeleteData(int id)
        {
            VMResponse response = roleServices.DeleteData(id);

            return response;
        }
    }
}
