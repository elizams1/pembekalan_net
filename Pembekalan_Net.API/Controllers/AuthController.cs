﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pembekalan_Net.API.Services;
using Pembekalan_Net.ViewModel;

namespace Pembekalan_Net.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private VMResponse response = new VMResponse();
        public AuthServices authServices;
        public AuthController(AuthServices _authServices)
        {
            authServices = _authServices;
        }

        [HttpGet("[action]/{email?}/{password?}")]
        public VMTblCustomer? CheckLogin(string email, string password)
        {
            VMTblCustomer? data = authServices.CheckLogin(email, password);
            return data;
        }

        [HttpGet("[action]/{idRole?}")]
        public List<VMMenuAccess> MenuAccess(int idRole)
        {
            List<VMMenuAccess> data = authServices.MenuAccess(idRole);
            return data;
        }
    }
}
