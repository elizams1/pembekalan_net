﻿using Pembekalan_Net.DataModel;
using Pembekalan_Net.Libraries;
using Pembekalan_Net.ViewModel;
using System.Data;

namespace Pembekalan_Net.API.Services
{
    public class CustomerServices
    {
        private readonly NewNetContext db;
        private VMResponse response = new VMResponse();
        int IdUser = 1;

        public CustomerServices(NewNetContext _db)
        {
            db=_db;
        }

        public List<VMTblCustomer> GetAllData()
        {
            //List<VMTblCustomer> data = new List<VMTblCustomer>();
            List<VMTblCustomer> dataModel = new List<VMTblCustomer> ();
            dataModel = (
                          from c in db.TblCustomers
                          join r in db.TblRoles
                            on c.IdRole equals r.Id
                          where c.IsDelete == false && r.IsDelete == false
                          select new VMTblCustomer
                          {
                              Id = c.Id,
                              NameCustomer = c.NameCustomer,
                              Email = c.Email,
                              Password = c.Password,
                              Address = c.Address,
                              Phone = c.Phone,
                              IdRole = c.IdRole,
                              RoleName = r.RoleName
                          }
                        ).ToList();
               

            return dataModel;
        }

        public VMTblCustomer? GetDataById(int id)
        {
            //List<VMTblCustomer> data = new List<VMTblCustomer>();
            VMTblCustomer? dataModel = (
                          from c in db.TblCustomers
                          join r in db.TblRoles
                            on c.IdRole equals r.Id
                          where c.Id == id && c.IsDelete == false && r.IsDelete == false
                          select new VMTblCustomer
                          {
                              Id = c.Id,
                              NameCustomer = c.NameCustomer,
                              Email = c.Email,
                              Password = EncryptDecrpytManager.Decrypt(c.Password),
                              Address = c.Address,
                              Phone = c.Phone,
                              IdRole = c.IdRole,
                              RoleName = r.RoleName
                          }
                        ).FirstOrDefault();

            return dataModel;
        }

        public VMResponse SaveData(TblCustomer data)
        {
            TblCustomer addData = new TblCustomer();
            addData.NameCustomer = data.NameCustomer;
            addData.Email = data.Email;
            addData.Password = EncryptDecrpytManager.Encrypt(data.Password);
            addData.Address = data.Address;
            addData.Phone = data.Phone;
            addData.IdRole = data.IdRole;
            addData.IsDelete = false;
            addData.CreateBy = IdUser;
            addData.CreateDate = DateTime.Now;

            try
            {
                db.Add(addData);
                db.SaveChanges();

                response.Message = "Data is successfully saved (created)";
                response.Entity = addData;

            }
            catch (Exception ex)
            {
                response.Message = "failed to save: " + ex.Message;
                response.Success = false;
                response.Entity = data;
            }

            return response;
        }

        public VMResponse EditData(TblCustomer data)
        {

            TblCustomer addData = db.TblCustomers.Find(data.Id);
            if(addData != null)
            {
                addData.NameCustomer = data.NameCustomer;
                addData.Email = data.Email;
                addData.Password = EncryptDecrpytManager.hashPassword(data.Password);
                addData.Address = data.Address;
                addData.Phone = data.Phone;
                addData.IdRole = data.IdRole;
                addData.IsDelete = false;
                addData.UpdateBy = IdUser;
                addData.UpdateDate = DateTime.Now;
            }
            

            try
            {
                db.Update(addData);
                db.SaveChanges();

                response.Message = "Data is successfully saved (updated)";
                response.Entity = addData;

            }
            catch (Exception ex)
            {
                response.Message = "failed to save: " + ex.Message;
                response.Success = false;
                response.Entity = data;
            }

            return response;
        }

        public VMResponse DeleteData(int id)
        {

            TblCustomer addData = db.TblCustomers.Find(id);
            if (addData != null)
            {   
                addData.IsDelete = true;
                addData.UpdateBy = IdUser;
                addData.UpdateDate = DateTime.Now;
            }


            try
            {
                db.Update(addData);
                db.SaveChanges();

                response.Message = "Data is successfully deleted";
                response.Entity = addData;

            }
            catch (Exception ex)
            {
                response.Message = "failed to save: " + ex.Message;
                response.Success = false;
                //response.Entity = data;
            }

            return response;
        }

        public VMResponse MultipleDeleteData(List<int> listId)
        {
            try
            {
                foreach (int id in listId)
                {
                    TblCustomer deleteData = db.TblCustomers.Find(id);
                    if (deleteData != null)
                    {
                        deleteData.IsDelete = true;
                        deleteData.UpdateBy = IdUser;
                        deleteData.UpdateDate = DateTime.Now;
                    }
                    db.Update(deleteData);
                }

                db.SaveChanges();
                response.Message = "Data is successfully deleted";
            }
            catch(Exception ex)
            {
                response.Message = "failed to save: " + ex.Message;
                response.Success = false;
            }
            
            return response;
        }

        public bool CheckEmailIsExist(string email, int id)
        {
            TblCustomer data = new TblCustomer();
            data = db.TblCustomers.Where(a=> a.Email==email && a.IsDelete==false).FirstOrDefault();

            if (data != null && data.Id != id)
            {
                return true;
            }
            return false;
        }
    }
}
