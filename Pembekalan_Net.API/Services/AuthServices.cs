﻿using Microsoft.AspNetCore.Rewrite;
using Pembekalan_Net.DataModel;
using Pembekalan_Net.ViewModel;

namespace Pembekalan_Net.API.Services
{
    public class AuthServices
    {
        private readonly NewNetContext db;
        private VMResponse response = new VMResponse();
        public AuthServices(NewNetContext _db) { 
            db = _db;
        }

        public VMTblCustomer? CheckLogin(string email, string password)
        {
            VMTblCustomer? dataModel = (
                          from c in db.TblCustomers
                          join r in db.TblRoles
                            on c.IdRole equals r.Id
                          where c.Email.Equals(email) && c.Password.Equals(password) &&c.IsDelete == false && r.IsDelete == false
                          select new VMTblCustomer
                          {
                              Id = c.Id,
                              NameCustomer = c.NameCustomer,
                              Email = c.Email,
                              Password = c.Password,
                              Address = c.Address,
                              Phone = c.Phone,
                              IdRole = c.IdRole,
                              RoleName = r.RoleName
                          }
                        ).FirstOrDefault();

            return dataModel;
        }

        public List<VMMenuAccess> MenuAccess(int idRole)
        {
            List<VMMenuAccess> data = (from m in db.TblMenus
                                       join ma in db.TblMenuAccesses on m.Id equals ma.MenuId
                                       where m.IsDelete == false && ma.IsDelete==false && m.IsParent == true && ma.RoleId == idRole
                                       select new VMMenuAccess
                                       {
                                           Id = m.Id,
                                           MenuName = m.MenuName,
                                           MenuAction = m.MenuAction,
                                           MenuController = m.MenuController,
                                           MenuIcon = m.MenuIcon,
                                           IdRole = ma.RoleId,
                                           MenuSorting = m.MenuSorting,
                                           ListChild = (from child in db.TblMenus
                                                        join ma2 in db.TblMenuAccesses on child.Id equals ma2.MenuId
                                                        where child.MenuParent == m.Id && child.IsDelete == false
                                                        && ma2.IsDelete == false && ma2.RoleId == idRole
                                                        select new VMMenuAccess
                                                        {
                                                            Id = child.Id,
                                                            MenuName = child.MenuName,
                                                            MenuAction = child.MenuAction,
                                                            MenuController = child.MenuController,
                                                            MenuIcon = child.MenuIcon,
                                                            IdRole = idRole,
                                                            MenuSorting = child.MenuSorting,
                                                            MenuParent = child.MenuParent,

                                                        }).OrderBy(a => a.MenuSorting).ToList()

                                       }).OrderBy(a => a.MenuSorting).ToList();

            return data;
        }
    }
}
