﻿using Pembekalan_Net.DataModel;
using Pembekalan_Net.ViewModel;
using System.Data;

namespace Pembekalan_Net.API.Services
{
    public class RoleServices
    {
        private readonly NewNetContext db;
        private VMResponse response = new VMResponse();
        int IdUser = 1;

        public RoleServices(NewNetContext _db)
        {
            db=_db;
        }

        public List<TblRole> GetAllData()
        {
            //List<VMTblRole> data = new List<VMTblRole>();
            List<TblRole> dataModel = db.TblRoles.Where(a => a.IsDelete == false).ToList();

            return dataModel;
        }

        public TblRole GetDataById(int id)
        {
            //List<VMTblRole> data = new List<VMTblRole>();
            TblRole dataModel = db.TblRoles.Where(g=>g.IsDelete == false && g.Id==id).FirstOrDefault();

            return dataModel;
        }

        public VMResponse SaveData(TblRole data)
        {
            TblRole addData = new TblRole();
            addData.RoleName = data.RoleName;
            addData.IsDelete = false;
            addData.CreatedBy = IdUser;
            addData.CreatedDate = DateTime.Now;

            try
            {
                db.Add(addData);
                db.SaveChanges();

                response.Message = "Data is successfully saved (created)";
                response.Entity = addData;

            }
            catch (Exception ex)
            {
                response.Message = "failed to save: " + ex.Message;
                response.Success = false;
                response.Entity = data;
            }

            return response;
        }

        public VMResponse EditData(TblRole data)
        {

            TblRole addData = GetDataById(data.Id);
            if(addData != null)
            {
                addData.RoleName = data.RoleName;
                addData.IsDelete = false;
                addData.UpdatedBy = IdUser;
                addData.UpdatedDate = DateTime.Now;
            }
            

            try
            {
                db.Update(addData);
                db.SaveChanges();

                response.Message = "Data is successfully saved (updated)";
                response.Entity = addData;

            }
            catch (Exception ex)
            {
                response.Message = "failed to save: " + ex.Message;
                response.Success = false;
                response.Entity = data;
            }

            return response;
        }

        public VMResponse DeleteData(int id)
        {

            TblRole addData = GetDataById(id);
            if (addData != null)
            {   
                addData.IsDelete = true;
                addData.UpdatedBy = IdUser;
                addData.UpdatedDate = DateTime.Now;
            }


            try
            {
                db.Update(addData);
                db.SaveChanges();

                response.Message = "Data is successfully deleted";
                response.Entity = addData;

            }
            catch (Exception ex)
            {
                response.Message = "failed to save: " + ex.Message;
                response.Success = false;
                //response.Entity = data;
            }

            return response;
        }

        public bool CheckNameIsExist(string name, int id)
        {
            TblRole data = new TblRole();
            data = db.TblRoles.Where(a=> a.RoleName==name && a.IsDelete==false).FirstOrDefault();

            if (data != null)
            {
                return true;
            }
            return false;
        }
    }
}
