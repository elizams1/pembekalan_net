﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Pembekalan_Net.DataModel
{
    [Table("TblMenu")]
    public partial class TblMenu
    {
        [Key]
        public int Id { get; set; }
        [StringLength(80)]
        public string? MenuName { get; set; }
        [StringLength(80)]
        public string? MenuAction { get; set; }
        [StringLength(80)]
        public string? MenuController { get; set; }
        [StringLength(80)]
        public string? MenuIcon { get; set; }
        [StringLength(80)]
        public int? MenuSorting { get; set; }
        public bool? IsParent { get; set; }
        public int? MenuParent { get; set; }
        public bool? IsDelete { get; set; }
        public int? CreateBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
