﻿//using System.Runtime.Intrinsics.Arm;
using System.Security.Cryptography;
using System.Text;

namespace Pembekalan_Net.Libraries
{
    public class EncryptDecrpytManager
    {
        //Use Scrypt(Cryptography) or Bscrypt


        //Security Cryptography Encrypt Decrypt (AES/Advanced Encryption Standard)
        public readonly static string key = "HelloWorld!00000";

        public static string Encrypt(string text)
        {
            byte[] iv = new byte[16];
            byte[] array;
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key,aes.IV);
                using(MemoryStream ms = new MemoryStream())
                {
                    using(CryptoStream cs = new CryptoStream((Stream)ms, encryptor, CryptoStreamMode.Write))
                    {
                        using(StreamWriter sw = new StreamWriter(cs))
                        {
                            sw.Write(text);
                        }
                        array= ms.ToArray();
                    }
                }

            }
            return Convert.ToBase64String(array);
        }

        public static string Decrypt(string text)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(text);
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream ms = new MemoryStream(buffer))
                {
                    using (CryptoStream cs = new CryptoStream((Stream)ms, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader sr = new StreamReader(cs))
                        {
                            return sr.ReadToEnd();
                        }
                    }
                }

            }
        }


        //Security Cryptography SHA256
        public static string hashPassword(string text)
        {
            var sha = SHA256.Create();
            var byteAArray = Encoding.Default.GetBytes(text);

            var hashedPassword = sha.ComputeHash(byteAArray);
            return Convert.ToBase64String(hashedPassword);
        }
    }
}
