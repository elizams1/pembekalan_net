﻿using Microsoft.AspNetCore.Mvc;
using Pembekalan_Net.DataModel;
using Pembekalan_Net.Services;
using Pembekalan_Net.ViewModel;
using System.Data;

namespace Pembekalan_Net.Controllers
{
    public class RoleValController : Controller
    {
        //private readonly NewNetContext db;
        private readonly RoleValServices roleValService;

        public RoleValController(RoleValServices _roleValService)
        {
            roleValService = _roleValService;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, int? pageSize, int? pageNumber, string currentFilter)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if(searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<TblRole> data = await roleValService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.RoleName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a=>a.RoleName).ToList(); 
                    break;
                default:
                    data = data.OrderBy(a => a.RoleName).ToList(); 
                    break;
            }

            return View(Pagination<TblRole>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
            
        }

        public IActionResult Create()
        {
            return PartialView();
        }

        public async Task<JsonResult> CheckNameIsExist(string name, int id)
        {
            bool isExist = await roleValService.CheckNameIsExist(name, id);
            return Json(isExist);
        }


        [HttpPost]
        public async Task<IActionResult> Create(TblRole data)
        {
            VMResponse response = new VMResponse();

            response = await roleValService.Create(data);

            if (response.Success)
            {
                return Json(response);
            }
            
            return View(data);
            
        }

        public async Task<IActionResult> Edit(int id)
        {
            TblRole data = await roleValService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TblRole data)
        {
            VMResponse response = new VMResponse();

            response = await roleValService.Edit(data);

            if (response.Success)
            {
                return Json(response);
            }

            return View(data);

        }

        public async Task<IActionResult> Detail(int id)
        {
            TblRole data = await roleValService.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TblRole data = await roleValService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteRole(int id)
        {
            VMResponse response = new VMResponse();

            response = await roleValService.Delete(id);

            if (response.Success)
            {
                return Json(response);
            }

            return RedirectToAction("Index");

        }
    }
}