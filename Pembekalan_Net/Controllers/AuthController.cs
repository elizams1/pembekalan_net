﻿using Microsoft.AspNetCore.Mvc;
using Pembekalan_Net.Services;
using Pembekalan_Net.ViewModel;

namespace Pembekalan_Net.Controllers
{
    public class AuthController : Controller
    {
        private readonly CustomerServices customerServices;
        private readonly AuthServices authServices;
        private readonly RoleValServices roleValServices;
        VMResponse response = new VMResponse();

        public AuthController(CustomerServices _customerServices, AuthServices _authServices, RoleValServices _roleValServices)
        {
            customerServices = _customerServices;
            authServices = _authServices;
            roleValServices = _roleValServices;

        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Login()
        {
            return PartialView();
        }
        public async Task<IActionResult> Register()
        {
            ViewBag.ListRoleId = await roleValServices.GetAllData();

            return PartialView();
        }

        [HttpPost]
        public async Task<JsonResult> CheckLogin(string email, string password)
        {
            VMTblCustomer data = await authServices.CheckLogin(email, password);

            if(data != null)
            {
                response.Success = true;
                response.Message = data.NameCustomer;
                HttpContext.Session.SetString("nameCustomer", data.NameCustomer);
                HttpContext.Session.SetInt32("idRole", data.IdRole ?? 0);
                HttpContext.Session.SetInt32("idCustomer", data.Id);
            }
            else
            {
                response.Success = false;
                response.Message = $"Maaf, {email} tidak ditemukan atau salah password!";
            }
            return Json(response);

        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return Redirect("/Home/Index");
        }
        
    }
}
