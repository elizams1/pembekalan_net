﻿using Microsoft.AspNetCore.Mvc;
using Pembekalan_Net.DataModel;
using Pembekalan_Net.Services;
using Pembekalan_Net.ViewModel;
using System.Collections.Generic;
using System.Data;

namespace Pembekalan_Net.Controllers
{
    public class CustomerController : Controller
    {
        //private readonly NewNetContext db;
        private readonly CustomerServices customerServices;
        private readonly RoleValServices roleValServices;

        //ctor : pembuatan constructor

        public CustomerController(CustomerServices _customerServices, RoleValServices _roleValServices)
        {
            customerServices = _customerServices;
            roleValServices = _roleValServices;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, int? pageSize, int? pageNumber, string currentFilter)
        {
            //ViewBag.CurrentSortRoleName = sortOrderRoleName;
            //ViewBag.StatusSortRoleName = string.IsNullOrEmpty(sortOrderRoleName) ? "roleName_desc" : "";

            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize == null ? 5 : pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.RoleName = sortOrder == "role" ? "role_desc" : "role";


            if(searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMTblCustomer> data = await customerServices.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameCustomer.ToLower().Contains(searchString.ToLower()) || a.RoleName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameCustomer).ToList();
                    break;
                case "roleName_desc":
                    data = data.OrderByDescending(a => a.RoleName).ToList();
                    break;
                case "role":
                    data = data.OrderBy(a => a.RoleName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameCustomer).ToList();
                    break;
            }
            
            return View(Pagination<VMTblCustomer>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 5));
            
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.ListRoleId =  await roleValServices.GetAllData();
            return PartialView();
        }

        public async Task<JsonResult> CheckEmailIsExist(string email, int id)
        {
            bool isExist = await customerServices.CheckEmailIsExist(email, id);
            return Json(isExist);
        }


        [HttpPost]
        public async Task<IActionResult> Create(TblCustomer data)
        {
            VMResponse response = new VMResponse();
            
            

            response = await customerServices.Create(data);

            if (response.Success)
            {
                return Json(response);
            }
            
            return View(data);
            
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMTblCustomer data = await customerServices.GetDataById(id);
            ViewBag.ListRoleId = await roleValServices.GetAllData();
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TblCustomer data)
        {
            VMResponse response = new VMResponse();

            response = await customerServices.Edit(data);

            if (response.Success)
            {
                return Json(response);
            }

            return View(data);

        }

        public async Task<IActionResult> Detail(int id)
        {
            VMTblCustomer data = await customerServices.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMTblCustomer data = await customerServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            VMResponse response = new VMResponse();

            response = await customerServices.Delete(id);

            if (response.Success)
            {
                return Json(response);
            }

            return RedirectToAction("Index");

        }

        public async Task<IActionResult> MultipleDelete(List<int> listId)
        {
            List<string> data = new List<string>();

            for (var i = 0; i < listId.Count; i++)
            {
                VMTblCustomer theId = await customerServices.GetDataById(listId[i]);
                data.Add(theId.NameCustomer);
            }
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureMultipleDelete(List<int> listId)
        {
            VMResponse response = new VMResponse();

            response = await customerServices.MultipleDelete(listId);

            if (response.Success)
            {
                return Json(response);
            }

            return RedirectToAction("Index");

        }

    }
}