﻿using Microsoft.AspNetCore.Mvc;
using Pembekalan_Net.DataModel;
using Pembekalan_Net.Services;
using Pembekalan_Net.ViewModel;

namespace Pembekalan_Net.Controllers
{
    public class RoleController : Controller
    {
        //private readonly NewNetContext db;
        private readonly RoleServices roleService;

        public RoleController(RoleServices _roleServices)
        {
            //db = _db;
            roleService = _roleServices;
        }

        public IActionResult Index()
        {
            List<VMTblRole> dataView = roleService.GetAllData();
            ViewBag.listRole = dataView;
            return View(dataView);
        }

        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public IActionResult Create(VMTblRole dataView)
        {
            VMResponse response = new VMResponse();

            if (ModelState.IsValid)
            {
                response = roleService.Create(dataView);

                if (response.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            response.Entity = dataView;
            return View(response.Entity);
        }

        public IActionResult Edit(int id)
        {
            VMTblRole dataView = roleService.GetById(id);
            return PartialView(dataView);
        }

        [HttpPost]
        public IActionResult Edit(VMTblRole dataView)
        {
            VMResponse response = new VMResponse();

            if (ModelState.IsValid)
            {
                response = roleService.Edit(dataView);

                if (response.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            response.Entity = dataView;
            return View(response.Entity);
        }

        public IActionResult Detail(int id)
        {
            VMTblRole dataView = roleService.GetById(id);
            return PartialView(dataView);
        }
        public IActionResult Delete(int id)
        {
            VMTblRole dataView = roleService.GetById(id);
            return PartialView(dataView);
        }

        [HttpPost]
        public VMResponse DeleteRole(int id)
        {
            VMResponse response = new VMResponse();

            response = roleService.Delete(id);
            return response;
        }

        //DELETE DENGAN CARA KEDUA
        //[HttpPost]
        //public IActionResult Delete(VMTblRole dataView)
        //{
        //    VMResponse response = new VMResponse();

        //    response = roleService.Edit(dataView);

        //    if (response.Success)
        //    {
        //        return RedirectToAction("Index");
        //    }
            
        //    response.Entity = dataView;
        //    return View(response.Entity);
        //}
    }
}