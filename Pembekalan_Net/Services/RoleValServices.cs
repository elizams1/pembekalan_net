﻿using AutoMapper;
using Newtonsoft.Json;
using Pembekalan_Net.DataModel;
using Pembekalan_Net.ViewModel;
using System.Data;
using System.Net.Http.Headers;
using System.Text;

namespace Pembekalan_Net.Services
{
    public class RoleValServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string ApiUrl;
        
        VMResponse response = new VMResponse();
        int IdUser = 1;


        public RoleValServices(IConfiguration _config)
        {
            configuration = _config;
            ApiUrl = configuration["ApiUrl"];
        }


        public async Task<List<TblRole>> GetAllData()
        {

            List<TblRole> data = new List<TblRole>();
            string apiResponse = await client.GetStringAsync(ApiUrl + "Role/GetAllData");
            data = JsonConvert.DeserializeObject<List<TblRole>>(apiResponse);

            return data;
        }

        public async Task<TblRole> GetDataById(int id)
        {

            TblRole data = new TblRole();
            string apiResponse = await client.GetStringAsync(ApiUrl + $"Role/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<TblRole>(apiResponse);

            return data;
        }

        public async Task<bool> CheckNameIsExist(string name, int id)
        {
            string apiResponse = await client.GetStringAsync(ApiUrl + $"Role/CheckNameIsExist/{name}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isExist;
        }

        public async Task<VMResponse> Create(TblRole data)
        {
            //Proses convert object ke string
            string thedata = JsonConvert.SerializeObject(data);

            //Proses mengubah string json sebagai req body
            StringContent content = new StringContent(thedata, UnicodeEncoding.UTF8, "application/json");

            //Proses memanggil API dan mengirimkan request body
            var apiResponse = await client.PostAsync(ApiUrl + "Role/SaveData", content);
            
            if(apiResponse.IsSuccessStatusCode)
            {
                //Proses membaca response dari API
                var res = await apiResponse.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                response = JsonConvert.DeserializeObject<VMResponse>(res);
            }
            else
            {
                response.Success= false;
                response.Message = $"{apiResponse.StatusCode} : {apiResponse.RequestMessage}";
            }


            return response;

        }

        public async Task<VMResponse> Edit(TblRole data)
        {
            //Proses convert object ke string
            string thedata = JsonConvert.SerializeObject(data);

            //Proses mengubah string json sebagai req body
            StringContent content = new StringContent(thedata, UnicodeEncoding.UTF8, "application/json");

            //Proses memanggil API dan mengirimkan request body
            var apiResponse = await client.PutAsync(ApiUrl + "Role/EditData", content);

            if (apiResponse.IsSuccessStatusCode)
            {
                //Proses membaca response dari API
                var res = await apiResponse.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                response = JsonConvert.DeserializeObject<VMResponse>(res);
            }
            else
            {
                response.Success = false;
                response.Message = $"{apiResponse.StatusCode} : {apiResponse.RequestMessage}";
            }


            return response;

        }

        public async Task<VMResponse> Delete(int id)
        {
            ////Proses convert object ke string
            //string thedata = JsonConvert.SerializeObject(data);

            ////Proses mengubah string json sebagai req body
            //StringContent content = new StringContent(thedata, UnicodeEncoding.UTF8, "application/json");

            //Proses memanggil API dan mengirimkan request body
            var apiResponse = await client.DeleteAsync(ApiUrl + $"Role/DeleteData/{id}");

            if (apiResponse.IsSuccessStatusCode)
            {
                //Proses membaca response dari API
                var res = await apiResponse.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                response = JsonConvert.DeserializeObject<VMResponse>(res);
            }
            else
            {
                response.Success = false;
                response.Message = $"{apiResponse.StatusCode} : {apiResponse.RequestMessage}";
            }


            return response;

        }


    }
}