﻿using AutoMapper;
using Newtonsoft.Json;
using Pembekalan_Net.DataModel;
using Pembekalan_Net.ViewModel;
using System.Data;
using System.Net.Http.Headers;
using System.Text;

namespace Pembekalan_Net.Services
{
    public class CustomerServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string ApiUrl;
        
        VMResponse response = new VMResponse();
        int IdUser = 1;


        public CustomerServices(IConfiguration _config)
        {
            configuration = _config;
            ApiUrl = configuration["ApiUrl"];
        }


        public async Task<List<VMTblCustomer>> GetAllData()
        {

            List<VMTblCustomer> data = new List<VMTblCustomer>();
            string apiResponse = await client.GetStringAsync(ApiUrl + "Customer/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblCustomer>>(apiResponse);

            return data;
        }

        public async Task<VMTblCustomer> GetDataById(int id)
        {

            VMTblCustomer data = new VMTblCustomer();
            string apiResponse = await client.GetStringAsync(ApiUrl + $"Customer/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblCustomer>(apiResponse);

            return data;
        }

        public async Task<bool> CheckEmailIsExist(string email, int id)
        {
            string apiResponse = await client.GetStringAsync(ApiUrl + $"Customer/CheckEmailIsExist/{email}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isExist;
        }

        public async Task<VMResponse> Create(TblCustomer data)
        {
            //Proses convert object ke string
            string thedata = JsonConvert.SerializeObject(data);

            //Proses mengubah string json sebagai req body
            StringContent content = new StringContent(thedata, UnicodeEncoding.UTF8, "application/json");

            //Proses memanggil API dan mengirimkan request body
            var apiResponse = await client.PostAsync(ApiUrl + "Customer/SaveData", content);
            
            if(apiResponse.IsSuccessStatusCode)
            {
                //Proses membaca response dari API
                var res = await apiResponse.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                response = JsonConvert.DeserializeObject<VMResponse>(res);
            }
            else
            {
                response.Success= false;
                response.Message = $"{apiResponse.StatusCode} : {apiResponse.RequestMessage}";
            }


            return response;

        }

        public async Task<VMResponse> Edit(TblCustomer data)
        {
            //Proses convert object ke string
            string thedata = JsonConvert.SerializeObject(data);

            //Proses mengubah string json sebagai req body
            StringContent content = new StringContent(thedata, UnicodeEncoding.UTF8, "application/json");

            //Proses memanggil API dan mengirimkan request body
            var apiResponse = await client.PutAsync(ApiUrl + "Customer/EditData", content);

            if (apiResponse.IsSuccessStatusCode)
            {
                //Proses membaca response dari API
                var res = await apiResponse.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                response = JsonConvert.DeserializeObject<VMResponse>(res);
            }
            else
            {
                response.Success = false;
                response.Message = $"{apiResponse.StatusCode} : {apiResponse.RequestMessage}";
            }


            return response;

        }

        public async Task<VMResponse> Delete(int id)
        {
            ////Proses convert object ke string
            //string thedata = JsonConvert.SerializeObject(data);

            ////Proses mengubah string json sebagai req body
            //StringContent content = new StringContent(thedata, UnicodeEncoding.UTF8, "application/json");

            //Proses memanggil API dan mengirimkan request body
            var apiResponse = await client.DeleteAsync(ApiUrl + $"Customer/DeleteData/{id}");

            if (apiResponse.IsSuccessStatusCode)
            {
                //Proses membaca response dari API
                var res = await apiResponse.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                response = JsonConvert.DeserializeObject<VMResponse>(res);
            }
            else
            {
                response.Success = false;
                response.Message = $"{apiResponse.StatusCode} : {apiResponse.RequestMessage}";
            }


            return response;

        }

        public async Task<VMResponse> MultipleDelete(List<int> listId)
        {
            //Proses convert object ke string
            string thedata = JsonConvert.SerializeObject(listId);

            //Proses mengubah string json sebagai req body
            StringContent content = new StringContent(thedata, UnicodeEncoding.UTF8, "application/json");

            //Proses memanggil API dan mengirimkan request body
            var apiResponse = await client.PostAsync(ApiUrl + $"Customer/MultipleDeleteData", content);

            if (apiResponse.IsSuccessStatusCode)
            {
                //Proses membaca response dari API
                var res = await apiResponse.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke Object
                response = JsonConvert.DeserializeObject<VMResponse>(res);
            }
            else
            {
                response.Success = false;
                response.Message = $"{apiResponse.StatusCode} : {apiResponse.RequestMessage}";
            }


            return response;

        }
    }
}