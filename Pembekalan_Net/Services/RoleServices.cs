﻿using AutoMapper;
using Pembekalan_Net.DataModel;
using Pembekalan_Net.ViewModel;
using System.Data;

namespace Pembekalan_Net.Services
{
    public class RoleServices
    {
        private readonly NewNetContext db;
        VMResponse response = new VMResponse();
        int IdUser = 1;


        public RoleServices(NewNetContext _db)
        {
            db = _db;
        }

        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblRole, VMTblRole>();
                cfg.CreateMap<VMTblRole, TblRole>();
            });
            IMapper mapper = config.CreateMapper();

            return mapper;
        }

        public List<VMTblRole> GetAllData()
        {
            List<VMTblRole> data = new List<VMTblRole>();
            List<TblRole> dataModel = db.TblRoles.Where(a => a.IsDelete == false).ToList();

            data = GetMapper().Map<List<VMTblRole>>(dataModel);

            //foreach(TblRole role in dataModel)
            //{
            //    VMTblRole item = new VMTblRole();
            //    item.Id = role.Id;
            //    item.RoleName = role.RoleName;
            //    item.IsDelete = role.IsDelete;
            //    item.CreatedBy = role.CreatedBy;
            //    item.CreatedDate = role.CreatedDate;
            //    item.UpdatedBy = role.UpdatedBy;
            //    item.UpdatedDate = role.UpdatedDate;

            //    data.Add(item);
            //}

            return data;
        }

        public VMTblRole GetById(int id)
        {
            //TblRole dataModel = db.TblRoles.Where(a => a.Id == id).FirstOrDefault()!;
            TblRole dataModel = db.TblRoles.Find(id)!;
            VMTblRole dataView = GetMapper().Map<VMTblRole>(dataModel);

            return dataView;
        }

        public VMResponse Create(VMTblRole dataView)
        {
            TblRole dataModel = GetMapper().Map<TblRole>(dataView);
            dataModel.IsDelete = false;
            dataModel.CreatedBy = IdUser;
            dataModel.CreatedDate = DateTime.Now;

            try
            {
                db.Add(dataModel);
                db.SaveChanges();

                response.Message = "Data is successfully saved (created)";
                response.Entity = dataModel;

            }
            catch (Exception ex)
            {
                response.Message = "failed to save: " + ex.Message;
                response.Success = false;
                response.Entity = dataView;
            }

            return response;
        }

        public VMResponse Edit(VMTblRole dataView)
        {
            TblRole dataModel = db.TblRoles.Find(dataView.Id);
            dataModel.RoleName = dataView.RoleName;
            dataModel.UpdatedBy = IdUser;
            dataModel.UpdatedDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                response.Message = "Data is successfully saved (updated)";
                response.Entity = dataModel;

            }
            catch (Exception ex)
            {
                response.Message = "failed to update: " + ex.Message;
                response.Success = false;
                response.Entity = dataView;
            }

            return response;
        }

        public VMResponse Delete(int id)
        {
            TblRole dataModel = db.TblRoles.Find(id);
            dataModel.IsDelete = true;
            dataModel.UpdatedBy = IdUser;
            dataModel.UpdatedDate = DateTime.Now;
            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                response.Message = "Data is successfully saved (deleted)";
                response.Entity = dataModel;

            }
            catch (Exception ex)
            {
                response.Message = "failed to delete: " + ex.Message;
                response.Success = false;
            }

            return response;
        }
    }
}