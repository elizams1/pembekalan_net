﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Pembekalan_Net.ViewModel;

namespace Pembekalan_Net.Services
{
    public class AuthServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string ApiUrl;

        public AuthServices(IConfiguration _config)
        {
            configuration = _config;
            ApiUrl = configuration["ApiUrl"];
        }

        public async Task<VMTblCustomer> CheckLogin(string email, string password)
        {
            VMTblCustomer data = new VMTblCustomer();
            string apiResponse = await client.GetStringAsync(ApiUrl + $"Auth/CheckLogin/{email}/{password}");
            data = JsonConvert.DeserializeObject<VMTblCustomer>(apiResponse);

            return data;
        }

        public async Task<List<VMMenuAccess>> MenuAccess(int idRole)
        {
            List<VMMenuAccess> data = new List<VMMenuAccess>();
            string apiResponse = await client.GetStringAsync(ApiUrl + $"Auth/MenuAccess/{idRole}");
            data = JsonConvert.DeserializeObject<List<VMMenuAccess>>(apiResponse);

            return data;
        }
    }
}
