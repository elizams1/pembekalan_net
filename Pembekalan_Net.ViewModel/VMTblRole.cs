﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pembekalan_Net.ViewModel
{
    public class VMTblRole
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Nama harap diisi")]
        [StringLength(10, MinimumLength =5)]
        public string? RoleName { get; set; }

        public bool? IsDelete { get; set; }
        public int CreatedBy {  get; set; }
        public DateTime CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        
    }
}
